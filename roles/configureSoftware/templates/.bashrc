{% if install_software_docker is defined %}
alias 'dkr-cmp=docker compose -f {{ docker_compose_folder }}/docker-compose.yml'
{% endif %}
alias 'ssh'='ssh -o VisualHostKey=yes -o ServerAliveInterval=20 -o ServerAliveCountMax=3'

alias 'ls'='ls -h --color=auto '

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

export PS1="\[$(tput bold)\]\[$(tput setaf 2)\]\u\[$(tput setaf 7)\]@\[$(tput setaf 2)\]\h\[$(tput setaf 7)\]:\n\[$(tput setaf 4)\]\W \\$ \[$(tput sgr0)\]"
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

export LC_CTYPE=en_US.UTF-8
[[ $- == *i* ]] && source /home/{{ current_user }}/.ssh/agent.sh
